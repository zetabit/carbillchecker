<?php
namespace App;
use Closure;
use Exception;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Throwable;
use App\DownloadsManager;
use Laravel\Dusk\Browser as DuskBrowser;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;

/**
 * TODO:
 * https://w3c.github.io/webdriver/webdriver-spec.html#proxy
 * 'proxy': {
'proxyType': 'manual',
'httpProxy': 'user:password@proxy.blah.co.uk:3128'
'sslProxy': 'user:password@proxy.blah.co.uk:3128'
}
 *
 *  http_proxy   http://user:password@proxy_url:port
https_proxy   http://user:password@proxy_url:port
 *
 */


/**
 * Reporting browser for console commands
 */
class Browser
{
    /**
     * @var DownloadsManager
     */
    public $downloadsManager;

    /**
     * @var DuskBrowser
     */
    private $browser;

    /**
     * Browser constructor.
     * @param \App\DownloadsManager $downloadsManager
     * @throws Exception
     */
    public function __construct(DownloadsManager $downloadsManager)
    {
        $this->downloadsManager = $downloadsManager;
    }

    /**
     * @param Closure $callback
     * @param null $initOptions
     * @throws Exception
     * @throws Throwable
     */
    public function browse(Closure $callback, $initOptions = null)
    {
        $initDriverOptions = $initOptions && isset($initOptions['driver']) ? $initOptions['driver'] : null;

        if (!$this->browser) {
            $this->browser = $this->newBrowser($this->createWebDriver($initDriverOptions));
        }
        try {
            $callback($this->browser);
        } catch (Exception $e) {
            $this->browser->screenshot('exception_' . date('Y_m_d_H_i_s'));
            throw $e;
        } catch (Throwable $e) {
            $this->browser->screenshot('exception_' . date('Y_m_d_H_i_s'));
            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    function __destruct()
    {
        if ($this->browser) {
            $this->closeBrowser();
        }
    }

    /**
     * @throws Exception
     */
    protected function closeBrowser()
    {
        if (!$this->browser) {
            throw new Exception("The browser hasn't been initialized yet");
        }
        $this->browser->quit();
        $this->browser = null;
    }

    protected function newBrowser($driver)
    {
        return new DuskBrowser($driver);
    }

    /**
     * Create the remote web driver instance.
     *
     * @param null $initOptions
     * @return RemoteWebDriver
     * @throws Exception
     */
    protected function createWebDriver($initOptions = null)
    {
        return retry(5, function () use ($initOptions) {
            $this->startPhantomJs();
            return $this->driver($initOptions);
        }, 150);
    }

    /**
     * @param null $initOptions
     * @return RemoteWebDriver
     */
    protected function driver($initOptions = null)
    {
        $options = new ChromeOptions();
        $prefs = ['download.default_directory' => $this->downloadsManager->getDownloadsPath()];
        $options->setExperimentalOption('prefs', $prefs);
        $options->addArguments([
          '--disable-gpu',
          '--headless',
          '--no-sandbox'
        ]);
        $capabilities = array(
          WebDriverCapabilityType::BROWSER_NAME => 'phantomjs',
          'phantomjs.page.settings.userAgent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86; rv:25.0) Gecko/20100101 Firefox/25.0',
          WebDriverCapabilityType::ACCEPT_SSL_CERTS => 'true',
          WebDriverCapabilityType::PLATFORM => 'LINUX',
          WebDriverCapabilityType::JAVASCRIPT_ENABLED => 'true',
          '--web-security' => 'no',
          '--ignore-ssl-errors' => 'YES',
          '--webdriver-loglevel' => 'DEBUG',
          'phantomjs.page.settings.webSecurityEnabled' => 'false',
          WebDriverCapabilityType::WEB_STORAGE_ENABLED => 'true',
        );
        $capabilities = new DesiredCapabilities($capabilities);
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        if($initOptions && $initOptions['proxy']) $capabilities->setCapability(WebDriverCapabilityType::PROXY, $initOptions['proxy']);
        $driver = RemoteWebDriver::create(
          'http://127.0.0.1:9515/wd/hub', $capabilities, 15000, 15000
        );
        $driver->manage()->window()->maximize();
        return $driver;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function startPhantomJs()
    {
        $pid = file_exists(config('phantomjs.pid_file')) ? file_get_contents(config('phantomjs.pid_file')) : 0;

        if ($pid > 0 && $this->isRunning($pid)) return true;

        try {

            if(file_exists(config('phantomjs.pid_file'))) unlink(config('phantomjs.pid_file'));

            exec(
              sprintf(
                '%s > %s 2>&1 & echo $! >> %s',
                config('phantomjs.binary_path') . ' ' .implode(" ", config('phantomjs.options')),
                config('phantomjs.log_file'),
                config('phantomjs.pid_file'))
            );

        } catch (\Exception $e) {

            \Log::info('startPhantomJs exception, try kill');

            $pid = file_exists(config('phantomjs.pid_file')) ? file_get_contents(config('phantomjs.pid_file')) : 0;

            if($pid > 0) $this->kill($pid);

            if(file_exists(config('phantomjs.pid_file'))) unlink(config('phantomjs.pid_file'));

            throw new Exception(sprintf('Error when executing PhantomJs procedure - %s', $e->getMessage()));
        }
    }

    protected function isRunning($pid){
        try {
            $result = shell_exec(sprintf("ps %d", $pid));
            if( count(preg_split("/\n/", $result)) > 2){
                return true;
            }
        } catch (Exception $e){
            \Log::info('error isRunning pid:' . $pid);
        }

        return false;
    }

    public function kill($pid = null){
        try {
            if(file_exists(config('phantomjs.pid_file'))) {
                $pid = file_get_contents(config('phantomjs.pid_file'));
                exec('kill -9 '.$pid);
                unlink(config('phantomjs.pid_file'));
            }
        } catch (Exception $e){
            \Log::error('kill pid:' . $pid);
        }
    }
}