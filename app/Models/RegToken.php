<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegToken extends Model
{
    protected $fillable = [
      'token', 'attempts'
    ];

    protected $casts = [
      'attempts'    => 'int'
    ];
}
