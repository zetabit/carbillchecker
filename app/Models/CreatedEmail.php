<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CreatedEmail
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $source
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $created_questionnaire_id
 * @property-read \App\Models\CreatedQuestionnaire $createdQuestionnaire
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail whereCreatedQuestionnaireId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedEmail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Car[] $cars
 */
class CreatedEmail extends Model
{
  const SOURCE_MAIL_RU = 'mail_ru', SOURCE_RAMBLER = 'rambler', SOURCE_GOOGLE = 'google', SOURCE_YANDEX = 'yandex';

  protected $fillable = [
    'email', 'password', 'source', 'created_questionnaire_id', 'user_id'
  ];

  public function createdQuestionnaire(){
      return $this->belongsTo(CreatedQuestionnaire::class);
  }

  public function cars(){
      return $this->hasMany(Car::class);
  }
}
