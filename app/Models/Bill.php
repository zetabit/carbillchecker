<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Bill
 *
 * @property int $id
 * @property float $number
 * @property string $from
 * @property string $StAP
 * @property string $place_of_drawing
 * @property mixed $date_time_violation
 * @property string $place_of_violation
 * @property string $authority
 * @property string $violator
 * @property string $ts
 * @property string $date_effective
 * @property string $type
 * @property float $price_no_discount
 * @property float $price_with_discount
 * @property mixed $date_time_discount_deadline
 * @property string $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereStAP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereAuthority($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereDateEffective($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereDateTimeDiscountDeadline($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereDateTimeViolation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill wherePlaceOfDrawing($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill wherePlaceOfViolation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill wherePriceNoDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill wherePriceWithDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereTs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereViolator($value)
 * @mixin \Eloquent
 * @property string $uid
 * @property int $car_id
 * @property-read \App\Models\Car $car
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereCarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereUid($value)
 */
class Bill extends Model
{
    const STATUS_NOT_PAID = 'not_paid', STATUS_PENDING = 'pending', STATUS_PAID = 'paid';

    protected $fillable = [
        'uid',
        'number',
        'from',
        'StAP',                 //Статья КоАП или закона субъекта РФ, состав правонарушения
        'place_of_drawing',     //Место составления документа
        'date_time_violation',  //Дата и время нарушения
        'place_of_violation',   //Место нарушения
        'authority',            //Орган власти, выявивший нарушение
        'violator',             //Нарушитель
        'ts',                   //транспортное средство
        'date_effective',       //дата вступления в силу
        'type',                 //тип
        'price_no_discount',
        'price_with_discount',
        'date_time_discount_deadline',
        'status',
        'car_id'
    ];

    protected $casts = [
        'date_time_violation'   =>  'dateTime',
        'date_time_discount_deadline'   =>  'dateTime',
    ];

    public function __construct(array $attributes = [])
    {
        $this->violator = "";
        $this->StAP = "";
        $this->place_of_drawing = "";
        $this->place_of_violation = "";
        $this->authority = "";
        $this->date_time_violation = "1993-01-01 00:00:01";
        $this->ts = "";
        parent::__construct($attributes);
        $this->date_effective = "1993-01-01";
        $this->date_time_discount_deadline = Carbon::createFromFormat('Y.m.d', "1993.01.01");
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
