<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Proxy
 *
 * @mixin \Eloquent
 * @property int $id
 * @property int $ip
 * @property int $port
 * @property int $attempts
 * @property string $status
 * @options ['active', 'checking', 'disabled']
 * @property string $code
 * @property string $country
 * @property string $version
 * @options ['http', 'socks4', 'socks5']
 * @property string $anonymity
 * @options ['elite', 'anonymous', 'transparent']
 * @property int $google
 * @property int $https
 * @property string $lastChecked
 * @property int $is_free
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereAnonymity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereAttempts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereGoogle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereHttps($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereLastChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy wherePort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereVersion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Proxy whereIsFree($value)
 */
class Proxy extends Model
{
  public $timestamps = true;

  protected $fillable = [
    'ip', 'port', 'attempts', 'status', 'code',
    'country', 'version', 'anonymity', 'google',
    'https', 'lastChecked', 'is_free'
  ];
}
