<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CreatedQuestionnaire
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property \Carbon\Carbon $birthday
 * @property string $gender
 * @property string $district
 * @property string $question_text
 * @property string $question_answer
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereDistrict($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereQuestionAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereQuestionText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CreatedQuestionnaire whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CreatedQuestionnaire extends Model
{
  const GENDER_MALE = 'male', GENDER_FEMALE = 'female';

  protected $fillable = [
    'firstName', 'lastName', 'birthday', 'gender', 'district',
    'question_text', 'question_answer'
  ];

  protected $casts = [
    'birthday' => 'datetime'
  ];
}
