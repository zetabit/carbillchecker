<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Car
 *
 * @property int $id
 * @property int $created_email_id
 * @property string $sts
 * @property string $vin
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCreatedEmailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereSts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereVin($value)
 * @mixin \Eloquent
 * @property string $number
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Bill[] $bills
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereNumber($value)
 */
class Car extends Model
{
    protected $fillable = [
        'created_email_id',
        'sts',
        'vin',
        'number'
    ];

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }
}
