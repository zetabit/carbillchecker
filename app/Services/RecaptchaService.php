<?php

namespace App\Services;

use App\Services\Api\ApiService;
use App\User;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverDimension;
use Facebook\WebDriver\WebDriverPoint;
use GuzzleHttp\Client;

use GuzzleHttp\Exception\ClientException;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Laravel\Dusk\Browser;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;

class RecaptchaService
{
    /** @var Browser */
    private $browser = '';

    /** @var ApiService|null */
    private $apiService = null;

    private $dos_dir;

    private $tmp_dir;

    public function __construct(Browser $browser)
    {
        $dos_dir = Browser::$storeScreenshotsAt . DIRECTORY_SEPARATOR . 'dos';
        if (!file_exists($dos_dir)) mkdir($dos_dir);
        $this->dos_dir = $dos_dir;

        $tmp_dir = Browser::$storeScreenshotsAt . DIRECTORY_SEPARATOR . 'tmp';
        if (!file_exists($tmp_dir)) mkdir($tmp_dir);
        $this->tmp_dir = $tmp_dir;

        $this->browser = $browser;
        $this->apiService = new ApiService();
        return $this;
    }

    /**
     * Get frames and switch to default content.
     *
     * @throws \Exception
     * @return RemoteWebElement[]
     */
    protected function getFrames()
    {
        $browser = $this->browser;
        $browser->driver->switchTo()->defaultContent();
        $iframes = $browser->driver->findElements(WebDriverBy::cssSelector('iframe'));
        $mainIframe = null;
        $secondIframe = null;

        /** @var RemoteWebElement $iframe */
        foreach ($iframes as $iframe) {
            $src = $iframe->getAttribute('src');

            if (parse_url($src, PHP_URL_HOST) != 'www.google.com')
                continue;

            if(!$mainIframe) $mainIframe = $iframe;
            elseif(!$secondIframe) $secondIframe = $iframe; //TODO: xpath("//iframe[contains(@title,'проверка recaptcha')]")
        }

        if(!$mainIframe || !$secondIframe) {
            throw new \Exception('recaptha iframes not found');
        }

        return [
          $mainIframe,
          $secondIframe
        ];
    }

    protected function dosMessageExists()
    {
        $browser = $this->browser;

        $dos = null;

        try{
            $browser->driver->switchTo()->frame($this->getFrames()[1]);
            $browser->waitFor('.rc-doscaptcha-header', 2);
            $dos = $browser->driver->findElement(WebDriverBy::cssSelector('.rc-doscaptcha-header'));
        } catch (\Exception $exception) {
            ;
        }

        if($dos) {
            $browser->screenshot('dos/' . date('Y_m_d_H_i_s'));
            return true;
        }

        return false;
    }

    /**
     * @throws \Exception
     * @return array|bool
     */
    protected function frame2()
    {
        $browser = $this->browser;

        $browser->driver->switchTo()->frame($this->getFrames()[1]);

        $browser->pause(100);

        if($this->dosMessageExists()) {
            echo 'Dos message... Click checkbox and await 15 sec...' . PHP_EOL;

            //$browser->driver->switchTo()->frame($this->getFrames()[0]);
            $browser->driver->switchTo()->defaultContent();
            $coords = $browser->driver->findElement(WebDriverBy::tagName('body'))->getCoordinates();
            $browser->driver->getMouse()
                ->mouseMove($coords, rand(10, 30), rand(20, 50))
                ->click();
            //$browser->driver->findElement(WebDriverBy::tagName('body'))->click();

            $browser->screenshot('dos' . DIRECTORY_SEPARATOR . date('Y_m_d_H_i_s_2'));
            sleep(15);
            $this->googleReCaptha();
            return false;
        }

        //check is query image is showed

        //TODO: try catch (if image is not present, for ex.)
        return $this->resolveCaptchaInBrowser();
    }

    /**
     * Get captcha image, instruction image, cols, rows.
     *
     * @throws \Exception
     */
    protected function resolveCaptchaInBrowser()
    {
        $browser = $this->browser;

        $browser->driver->switchTo()->frame($this->getFrames()[1]);
        $browser->waitFor('.rc-imageselect-desc-wrapper');
        //Получаем текст инструкции, url картинки и список квадратов на которые она разбита
        $src = $browser->driver->findElement(WebDriverBy::xpath("//img[contains(@class, 'rc-image-tile-')]"))->getAttribute('src');
        $description = $browser->driver->findElement(WebDriverBy::cssSelector('.rc-imageselect-desc-wrapper'))->getAttribute('textContent');

        $elements = $browser->driver->findElement(WebDriverBy::xpath("//table[contains(@class,'rc-imageselect-table-')]"))
            ->findElements(WebDriverBy::cssSelector('.rc-image-tile-wrapper'));

        //load captcha to variable
        $captchaContent = file_get_contents($src);
        // $name = date('Y_m_d_H_i_s');
        // $captchaLocalPath = $this->tmp_dir . DIRECTORY_SEPARATOR . 'forSend' . $name . '.png';
        // file_put_contents($captchaLocalPath, $captchaContent);

        //сохраняем инструкцию в виде картинки
        // $pathInstruction = $this->screenShotElement($this->getFrames()[1], '.rc-imageselect-instructions');
        // $instructionsContent = file_get_contents($pathInstruction);
        $instructionsContent = $browser->driver->findElement(WebDriverBy::className('rc-imageselect-instructions'))->getAttribute('textContent');

        //Определяем размерность сетки (может, размерностей больше, но пока я встречал только три типа)
        $columns = 1;
        $rows = 1;
        if (sizeof($elements) == 8) {
            $rows = 4;
            $columns = 2;
        } elseif (sizeof($elements) == 9) {
            $rows = 3;
            $columns = 3;
        } else {
            $rows = 4;
            $columns = 4;
        }


        $captchaContent = $this->setSheetOnImage($captchaContent, $columns, $rows);

        return [
          'captcha' => $captchaContent,
          'instruction' => $instructionsContent,
          'rows'    => $rows,
          'cols'    => $columns
        ];
    }

    public function setSheetOnImage($imContent, $cols, $rows) {
        $im = imagecreatefromstring($imContent);
        $width = imagesx($im);
        $height = imagesy($im);
        $colW = (int) ($width / $cols);
        $rowH = (int) ($height/ $rows);
        $color = imagecolorallocate($im, 255, 255, 255);

        for ($x = 0; $x < $width; $x += $colW) {
            for ($y = 0; $y < $height; $y += $rowH) {
                //вертикальные линии
                imageline($im, $x, $y, $x, $y + $rowH, $color);
                //горизонтальные линии
                imageline($im, $x, $y, $x + $colW, $y, $color);
            }
        }

        // start buffering
        ob_start();
        imagepng($im);
        $contents =  ob_get_contents();
        ob_end_clean();

        imagedestroy($im);
        return $contents;
    }

    /**
     * @param $code
     *
     * @return bool true есть следующие картинки
     * @throws \Exception
     */
    public function clickTiles($code) {
        $browser = $this->browser;
        $code = strtolower($code);
        $code = str_replace(['a', 'b', 'c', 'd', 'e', 'f'], [10, 11, 12, 13, 14, 15, 16], $code);
        $clickIndexes = str_split($code);

        $browser->driver->switchTo()->frame($this->getFrames()[1]);

        $clickElements = $browser->driver->findElement(WebDriverBy::xpath("//table[contains(@class,'rc-imageselect-table-')]"))
          ->findElements(WebDriverBy::cssSelector('.rc-image-tile-wrapper'));

        for ($i = 1, $c = sizeof($clickElements); $i <= $c; $i++) {
            if (! in_array($i, $clickIndexes)) continue;
            $element = $clickElements[$i - 1];
            $element->click();
            sleep(1);
        }

        $successBtn = $this->browser->driver->findElement(WebDriverBy::id("recaptcha-verify-button"));
        $successBtnText = $successBtn->getAttribute('textContent');
        // Далее
        $bNext = $successBtnText == "Далее";
        $successBtn->click();
        return $bNext;
    }

    /**
     * @param RemoteWebElement|null $iframe
     * @param string                $cssSelector
     *
     * @return string
     */
    public function screenShotElement(RemoteWebElement $iframe = null, $cssSelector, $maxSize = null)
    {
        $browser = $this->browser;
        $offsetX = 0; $offsetY = 0;
        if($iframe) {
            $offsetX = $iframe->getCoordinates()->onPage()->getX();
            $offsetY = $iframe->getCoordinates()->onPage()->getY();
            $this->browser->driver->switchTo()->frame($iframe);
        }
        $element = $browser->driver->findElement(WebDriverBy::cssSelector($cssSelector));
        $size = $element->getSize();
        $coordinates = $element->getCoordinates()->onPage();
        $capName = 'captcha_' . date('Y_m_d_H_i_s');
        $browser->screenshot('tmp' . DIRECTORY_SEPARATOR . $capName);
        $capPath = $this->tmp_dir . DIRECTORY_SEPARATOR . $capName . '.png';

        while (!file_exists($capPath)) {
            usleep(500);
        }

        $srcCap = imagecreatefrompng($capPath);
        $destCap = imagecreatetruecolor($size->getWidth(), $size->getHeight());
        imagecopy($destCap, $srcCap, 0, 0, $offsetX + $coordinates->getX(), $offsetY + $coordinates->getY(), $size->getWidth(), $size->getHeight());
        if ($destCap) {
            imagefilter($destCap, IMG_FILTER_GRAYSCALE);
        }
        imagepng($destCap, $this->tmp_dir . DIRECTORY_SEPARATOR . $capName . '_cut.png');
        imagedestroy($destCap); imagedestroy($srcCap);
       return $this->tmp_dir . DIRECTORY_SEPARATOR . $capName . '_cut.png';
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function googleReCaptha()
    {
        $browser = $this->browser;
        $browser->waitFor('iframe');
        $browser->screenshot('cap0');
        $mainFrame = $this->getFrames()[0];
        $coords = $mainFrame->getCoordinates()->onPage();
        $browser->driver->switchTo()->frame($mainFrame);

        //click checkbox
        $checkbox = $browser->driver->findElement(WebDriverBy::cssSelector('.recaptcha-checkbox-checkmark'));
        $browser->driver->getMouse()
            ->mouseMove($checkbox->getCoordinates(), $coords->getX(), $coords->getY())
            ->click();

        $res = false;

        //while next btn is present
        while( ($res = $this->resolveCaptcha()) ) {
            echo 'captcha variant (maybe) resolved... wait 5 sec...';
            sleep(5);
        }

        $browser->screenshot('cap1');

        return $res;
    }

    /**
     * @return array|bool
     * @throws \Exception
     */
    protected function resolveCaptcha()
    {
        $browser = $this->browser;

        $data = $this->frame2();

        if($data === false)
            return $data;

        file_put_contents($this->tmp_dir . DIRECTORY_SEPARATOR . 'tmp_' . date('Y_m_d_H_i_s') . '.png', $data['captcha']);

        //Ждем индексы картинок, по которым нужно кликнуть
        $id = $this->apiService->putReCaptchaV2($data['captcha'], $data['instruction'], $data['rows'], $data['cols']);

        echo 'Captcha ID: ' . $id . PHP_EOL;

        $code = null;
        $attempts = 30;

        do {
            echo date('Y-m-d H:i:s') . ' sleep 5 sec...' . PHP_EOL;
            sleep(5);

            $request = $this->apiService->getReCaptchaV2($id);

            if($request !== ApiService::CAPCHA_NOT_READY) {
                $code = $request;
                echo date('Y-m-d H:i:s') . ' cap resolved: ' . $code . PHP_EOL;
            }

        } while ($code == null && ($attempts-- > 0));

        $browser->driver->switchTo()->defaultContent();

        return $this->clickTiles($code);
    }

}