<?php

namespace App\Services\Api;

use App\User;
use GuzzleHttp\Client;

use GuzzleHttp\Exception\ClientException;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;

class ApiService
{
    const RUCAPTCHA_NOT_READY = 'CAPCHA_NOT_READY', CAPCHA_NOT_READY = 'CAPCHA_NOT_READY';

    private $client = null;

    private $apiPoint = '';

    /**
     * ApiService constructor.
     * @throws \Exception
     */
    public function __construct()
    {

        $config = [];
        if (getenv('APP_LOG_LEVEL') == 'debug') {
            $config = array_merge($this->initLogMessageFormatter(), $config);
        }
        $this->client = new Client($config);

        $this->apiPoint = env('APP_URL');

        return $this;
    }


    /**
     * @return array
     * @throws \Exception
     */
    private function initLogMessageFormatter()
    {
        $config = [];

        $stack = HandlerStack::create();
        $logger = new Logger('Logger');
        $logger->pushHandler(new StreamHandler(storage_path('logs/guzzle.log')));

        $stack->push(
          Middleware::log(
            $logger,
            new MessageFormatter('{method} {target} {req_body} - {res_body}')
          )
        );

        $config['handler'] = $stack;
        return $config;
    }

    /**
     * @param $method
     * @param $fields
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function jsonPost($method, $fields)
    {
        $body = json_encode($fields, JSON_PRETTY_PRINT);

        Log::info('Api POST ' . $method . " request: " . $body);

        $response = $this->jsonPostCustom($this->apiPoint . $method, $body);

        Log::info('Api POST ' . $method . " response: " . $response->getBody());

        return $response;
    }

    /**
     * @param $url
     * @param $body
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function jsonPostCustom($url, $body)
    {
        $response = $this->client->post($url, [
          'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
          'body' => $body,
        ]);

        return $response;
    }

    /**
     * @param $method
     * @param array $fields
     * @return \Psr\Http\Message\ResponseInterface
     */
    private function jsonGet($method, $fields = [])
    {
        Log::info('Api GET ' . $method . " " . json_encode($fields));

        $response = $this->client->get($this->apiPoint . $method, [
          'query' => $fields,
          'headers' => ['Accept' => 'application/json'],
        ]);

        Log::info('Api GET ' . $method . " response: " . $response->getBody());

        return $response;

    }

    /**
     * @param string $k google key
     * @param string $pageurl
     *
     * @return string id
     * @throws \Exception
     */
    public function putReCaptchaV2New($k, $pageurl)
    {
        $this->apiPoint = 'http://rucaptcha.com/';
        $response = $this->jsonPost('in.php', [
            'key'         =>  \config('rucaptcha.key'),
            'method'      =>  'userrecaptcha',
            'googlekey'   =>  $k,
            'pageurl'     =>  $pageurl,
            'json'        =>  1
        ]);

        $res = json_decode($response->getBody(), JSON_PRETTY_PRINT);

        if(!isset($res['status']) || $res['status'] !== 1) {
            throw new \Exception('Captcha in not putted.');
        }

        return $res['request'];
    }

    /**
     * @param string $captcha
     * @param string $textInstructions
     * @param int $rows
     * @param int $cols
     *
     * @return string id
     * @throws \Exception
     */
    public function putReCaptchaV2($captcha, $textInstructions, $rows, $cols)
    {
        $this->apiPoint = 'http://rucaptcha.com/';
        $response = $this->jsonPost('in.php', [
            'key'         =>  \config('rucaptcha.key'),
            'recaptcha'   => 1,
            'method'      =>  'base64',
            'body'        => base64_encode($captcha),
            'textinstructions' => $textInstructions,
            'recaptcharows' => $rows,
            'recaptchacols' => $cols,
            'json'        =>  1
        ]);

        $res = json_decode($response->getBody(), JSON_PRETTY_PRINT);

        if(!isset($res['status']) || $res['status'] !== 1) {
            throw new \Exception('Captcha in not putted.' . var_export($res, true));
        }

        return $res['request'];
    }

    /**
     * @param string $captcha
     *
     * @return string id
     * @throws \Exception
     */
    public function putCaptcha($captcha)
    {
        $this->apiPoint = 'http://rucaptcha.com/';
        $response = $this->jsonPost('in.php', [
            'key'         =>  \config('rucaptcha.key'),
            'method'      =>  'base64',
            'body'        => base64_encode($captcha),
            'json'        =>  1
        ]);

        $res = json_decode($response->getBody(), JSON_PRETTY_PRINT);

        if(!isset($res['status']) || $res['status'] !== 1) {
            throw new \Exception('Captcha in not putted.' . var_export($res, true));
        }

        return $res['request'];
    }

    /**
     * @param $captcha_id
     * @return mixed
     * @throws \Exception
     */
    public function getReCaptchaV2New($captcha_id)
    {
        $this->apiPoint = 'http://rucaptcha.com/';
        $response = $this->jsonGet('res.php', [
          'key'     => \config('rucaptcha.key'),
          'action'  => 'get',
          'id'      =>  $captcha_id,
          'json'        =>  1
        ]);

        $res = json_decode($response->getBody(), JSON_PRETTY_PRINT);

        if(!isset($res['status']) || ($res['status'] !== 1 && $res['request'] !== self::RUCAPTCHA_NOT_READY)) {
            throw new \Exception('Captcha in not got.' . $res['request']);
        }

        return $res['request'];
    }

    /**
     * @param $captcha_id
     * @return mixed
     * @throws \Exception
     */
    public function getReCaptchaV2($captcha_id)
    {
        $this->apiPoint = 'http://rucaptcha.com/';
        $response = $this->jsonGet('res.php', [
          'key'     => \config('rucaptcha.key'),
          'action'  => 'get',
          'id'      =>  $captcha_id,
          'json'        =>  1
        ]);

        $res = json_decode($response->getBody(), JSON_PRETTY_PRINT);

        if(!isset($res['status']) || ($res['status'] !== 1 && $res['request'] !== self::CAPCHA_NOT_READY)) {
            throw new \Exception('Captcha in not got.' . $res['request']);
        }

        return $res['request'];
    }

    /**
     * @param $captcha_id
     * @return mixed
     * @throws \Exception
     */
    public function getCaptcha($captcha_id)
    {
        $this->apiPoint = 'http://rucaptcha.com/';
        $response = $this->jsonGet('res.php', [
          'key'     => \config('rucaptcha.key'),
          'action'  => 'get',
          'id'      =>  $captcha_id,
          'json'        =>  1
        ]);

        $res = json_decode($response->getBody(), JSON_PRETTY_PRINT);

        if(!isset($res['status']) || ($res['status'] !== 1 && $res['request'] !== self::CAPCHA_NOT_READY)) {
            throw new \Exception('Captcha in not got.' . $res['request']);
        }

        return $res['request'];
    }

}