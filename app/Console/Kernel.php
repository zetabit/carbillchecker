<?php

namespace App\Console;

use App\Console\Commands\CreateRegTokenCommand;
use App\Console\Commands\CreateUserCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\BrowserDownloadExample::class,
        Commands\ExtractDataFromPageExample::class,
        Commands\proxyScrapperCommand::class,
        Commands\proxyNordScrapperCommand::class,
        Commands\phantomJsKill::class,
        Commands\recheckProxiesCommand::class,
        CreateUserCommand::class,
        CreateRegTokenCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
      // $schedule->command('inspire')
      //          ->hourly();

      //$schedule->command('browser:download')->hourly();
      //$schedule->command('browser:extract')->withoutOverlapping()->everyMinute();
      //$schedule->command('command.queue.work');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
