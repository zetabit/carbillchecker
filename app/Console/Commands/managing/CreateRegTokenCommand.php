<?php
namespace App\Console\Commands;
use App\Browser;
use App\Models\RegToken;
use App\User;
use League\Csv\Reader;
use Illuminate\Console\Command;
class CreateRegTokenCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'command:managing:create_tokens {count}';
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create token to avail reg user to this system.';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $count = $this->argument('count');

      for ($i = 0; $i < $count; $i++) {
          $regToken = RegToken::create([
                'token' =>  str_random(),
                'attempts'  => 1
          ]);
          echo $regToken->token . PHP_EOL;
      }
      echo 'Done.' . PHP_EOL;
  }
}