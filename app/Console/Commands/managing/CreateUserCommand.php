<?php
namespace App\Console\Commands;
use App\Browser;
use App\User;
use League\Csv\Reader;
use Illuminate\Console\Command;
class CreateUserCommand extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'command:managing:create_user {email} {password}';
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Create user to this system.';

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
      $email = $this->argument("email");
      $password = $this->argument("password");
      $hash = \Hash::make($password);

      $user = User::where('email', '=', $email)->first();
      if($user) {
          echo 'User with this email ' . $email . ' already created.' . PHP_EOL;
          return;
      }

      $user = new User([
            'email' => $email,
            'password'  => $hash
      ]);
      $user->save();
      $user->password = $hash;
      $user->save();

      echo 'User [' . $user->id .'] is created.' . PHP_EOL;
  }
}