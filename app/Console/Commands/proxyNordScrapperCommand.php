<?php

namespace App\Console\Commands;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\Proxy;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;

class proxyNordScrapperCommand extends Command
{
  protected $browser;

  protected $logger;

  protected $host;

  protected $more = 1000;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'command:proxyScrapperNord';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Scrap proxy list from proxy list.';

  /**
   * Create a new command instance.
   *
   * @param Browser $browser
   * @param LogToChannels $logger
   */
  public function __construct(Browser $browser, LogToChannels $logger)
  {
    parent::__construct();
    $this->browser = $browser;
    $this->logger  = $logger;
    $this->host = 'https://nordvpn.com/ru/free-proxy-list/';
  }

  protected function fromItem(RemoteWebElement $el) {
    /** @var RemoteWebElement[] $tds */
    $tds = $el->findElements(WebDriverBy::cssSelector('td'));

    if(sizeof($tds) < 4) return null;

    $ip   = $tds[1]->getAttribute('innerHTML');
    $port = $tds[2]->getAttribute('innerHTML');
    $code = "";
    $country=$tds[0]->getText();
    $anonymity = "anonymous";
    $google= false;
    $https =$tds[3]->getAttribute('innerHTML') == "HTTPS";
    $type = $https ? 'http' : 'http';

    if($tds[3]->getAttribute('innerHTML') == "SOCKS4") $type = 'socks4';
    if($tds[3]->getAttribute('innerHTML') == "SOCKS5") $type = 'socks5';

    if($anonymity == "elite proxy") $anonymity = 'elite';

    $proxy = Proxy::where([
      ['ip', '=', ip2long($ip)],
      ['port','=', (int)$port]
    ])->first();

    $proxy->country = $country;
    $proxy->version = $type;
    $proxy->save();

    if($proxy) return $proxy;

    $proxy = new Proxy([
      'ip'      => ip2long($ip),
      'port'    => (int)$port,
      'code'    => $code,
      'country' => $country,
      'anonymity' => $anonymity,
      'google'  => $google,
      'https'   => $https,
      'lastChecked' => date('Y-m-d H:i:s', 0),
      'version'    => $type
    ]);

    return $proxy;
  }

  protected function collectProxies(\Laravel\Dusk\Browser $browser)
  {
    //$browser->script('document.getElementById("proxylisttable_wrapper").scrollIntoView()');
    $table = $browser->element('table.proxy-list-table');
    sleep(3);
    /** @var RemoteWebElement[] $cols */
    $cols = $table->findElements(WebDriverBy::cssSelector('tbody tr'));

    foreach ($cols as $col) {
      $proxy = $this->fromItem($col);
      if(!$proxy) continue;
      $bNew = $proxy->id == null;
      $proxy->save();
      if($bNew) {
        $this->logger->info('ip_collect_nord', 'collected ip ' . long2ip($proxy->ip));
      }
    }

    if($this->more-- >= 0) {
      $a_more = $browser->element('div.load-more-holder a');

      if($a_more) {
        $this->logger->notice('ip_collect_nord', 'LOAD MORE');
        $a_more->click();
        sleep(2);
        $this->collectProxies($browser);
      }
    }
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->browser->browse(function ($browser) {

      /** @var \Laravel\Dusk\Browser $browser */
      $browser->visit($this->host);

      $this->collectProxies($browser);
      return true;
    });
  }
}
