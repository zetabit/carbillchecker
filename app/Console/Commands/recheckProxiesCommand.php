<?php

namespace App\Console\Commands;

use App\Jobs\checkProxyJob;
use App\Models\Proxy;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Console\Command;
use Illuminate\Database\Query\Builder;

class recheckProxiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:recheckProxies {reenable?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add all working proxies to recheck.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $bReenable = $this->argument('reenable') == true || $this->argument('reenable') == 1;

        $proxies = $bReenable ? Proxy::whereIn('status', ['checking', 'disabled'])->get() : Proxy::whereIn('status', ['checking'])->get();

        foreach ($proxies as $proxy) {
          if($bReenable && $proxy->status == 'disabled') {
            $proxy->status = 'checking';
            $proxy->attempts = 0;
            $proxy->save();
          }

          dispatch(new checkProxyJob($proxy));
        }
    }
}
