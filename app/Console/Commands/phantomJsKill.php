<?php

namespace App\Console\Commands;

use App\Browser;
use Illuminate\Console\Command;

class phantomJsKill extends Command
{
    protected $browser;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:phantomjs:kill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param Browser $browser
     * @return mixed
     */
    public function handle(Browser $browser)
    {
        $this->browser = $browser;
        $this->browser->kill();
    }
}
