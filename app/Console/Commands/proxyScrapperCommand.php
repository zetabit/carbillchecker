<?php

namespace App\Console\Commands;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\Proxy;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;

class proxyScrapperCommand extends Command
{
  protected $browser;

  protected $logger;

  protected $host;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'command:proxyScrapper';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Scrap proxy list from proxy list.';

  /**
   * Create a new command instance.
   *
   * @param Browser $browser
   * @param LogToChannels $logger
   */
  public function __construct(Browser $browser, LogToChannels $logger)
  {
    parent::__construct();
    $this->browser = $browser;
    $this->logger  = $logger;
    $this->host = 'https://free-proxy-list.net/anonymous-proxy.html';
  }

  protected function fromItem(RemoteWebElement $el) {
    /** @var RemoteWebElement[] $tds */
    $tds = $el->findElements(WebDriverBy::cssSelector('td'));

    if(sizeof($tds) <= 7) return null;

    $ip   = $tds[0]->getAttribute('innerHTML');
    $port = $tds[1]->getAttribute('innerHTML');
    $code = $tds[2]->getAttribute('innerHTML');
    $country=$tds[3]->getAttribute('innerHTML');
    $anonymity = $tds[4]->getAttribute('innerHTML');
    $google=$tds[5]->getAttribute('innerHTML') == "yes";
    $https =$tds[6]->getAttribute('innerHTML') == "yes";

    if($anonymity == "elite proxy") $anonymity = 'elite';

    $proxy = Proxy::where([
      ['ip', '=', ip2long($ip)],
      ['port','=', (int)$port]
    ])->first();

    if($proxy) return $proxy;

    $proxy = new Proxy([
      'ip'      => ip2long($ip),
      'port'    => (int)$port,
      'code'    => $code,
      'country' => $country,
      'anonymity' => $anonymity,
      'google'  => $google,
      'https'   => $https,
      'lastChecked' => date('Y-m-d H:i:s', 0)
    ]);

    return $proxy;
  }

  protected function collectProxies(\Laravel\Dusk\Browser $browser)
  {
    $browser->script('document.getElementById("proxylisttable_wrapper").scrollIntoView()');
    $table = $browser->element('#proxylisttable_wrapper table');
    /** @var RemoteWebElement[] $cols */
    $cols = $table->findElements(WebDriverBy::cssSelector('tbody tr[role=row]'));

    foreach ($cols as $col) {
      $proxy = $this->fromItem($col);
      if(!$proxy) continue;
      $bNew = $proxy->id == null;
      $proxy->save();
      if($bNew) {
        $this->logger->info('ip_collect', 'collected ip ' . long2ip($proxy->ip));
      }
    }

    $a_next_disabled = $browser->element('ul.pagination li#proxylisttable_next.disabled a');
    $a_next = $browser->element('ul.pagination li#proxylisttable_next a');

    if(!$a_next_disabled && $a_next) {
      $this->logger->notice('ip_collect', 'NEXT page');
      $a_next->click();
      $this->collectProxies($browser);
    }
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->browser->browse(function ($browser) {

      /** @var \Laravel\Dusk\Browser $browser */
      $browser->visit($this->host);

      $this->collectProxies($browser);
      return true;
    });
  }
}
