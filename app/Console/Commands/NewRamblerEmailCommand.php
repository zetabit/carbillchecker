<?php

namespace App\Console\Commands;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\CreatedEmail;
use App\Models\CreatedQuestionnaire;
use App\Services\Api\ApiService;
use App\Services\RecaptchaService;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;

class NewRamblerEmailCommand extends Command
{
    protected $browser;

    protected $logger;

    /** @var ApiService */
    protected $apiService;

    /** @var RecaptchaService */
    protected $recaptchaService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:mail:rambler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param Browser $browser
     * @param LogToChannels $logger
     * @throws \Exception
     */
    public function __construct(Browser $browser, LogToChannels $logger)
    {
        parent::__construct();
        $this->browser = $browser;
        $this->logger  = $logger;
        $this->apiService = new ApiService();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle()
    {
        $this->browser->browse(function(\Laravel\Dusk\Browser $browser){
            $recaptchaService = $this->recaptchaService = new RecaptchaService($browser);
            $browser->visit('https://www.google.com/recaptcha/api2/demo');

            $recaptchaService->googleReCaptha();

            return;
            $faker = \Faker\Factory::create('ru_RU');
            $createdEmail = new CreatedEmail([
              'source' => CreatedEmail::SOURCE_RAMBLER,
            ]);
            $questionnaire = new CreatedQuestionnaire();

            $browser->visit('https://id.rambler.ru/account/registration');

            $browser->waitFor('input[name=firstname]');

            $faker->firstName = 'Нонна';
            $questionnaire->firstName = $faker->firstName;
            $browser->keys('input[name=firstname]', $questionnaire->firstName);

            $faker->lastName = 'Ивановна';
            $questionnaire->lastName = $faker->lastName;
            $browser->keys('input[name=lastname]', $questionnaire->lastName);

            $browser->waitFor('[data-cerber-id="registration_form::mail::suggest"]');
            //$browser->waitForText('Свободные адреса');

            if (!$this->selectAvailEmail($browser, $createdEmail)) {
                echo "Avail email is not selected." . PHP_EOL;
                return false;
            }

            $browser->screenshot('src00');

            $browser->pause(rand(10, 50));

            if(
              $browser
                ->element('[data-cerber-id="registration_form::mail::mailbox_domain"]')
                ->getAttribute('value')
              !=
              "@rambler.ru"
            ) {
                echo 'It is not rambler :(' . PHP_EOL;
                $browser->screenshot('fail');
                return false;
            }

            $browser->pause(rand(10, 50));

            $createdEmail->password = str_random();

            $browser->keys('input[name="password.main"]', $createdEmail->password);

            $browser->pause(rand(10, 50));

            $browser->keys('input[name="password.confirm"]', $createdEmail->password);

            $browser->pause(rand(10, 50));

            //***     START BIRTHDAY

            $day = $this->selectRandomDay($browser, $createdEmail);

            $browser->pause(rand(10, 50));

            $month = $this->selectRandomMonth($browser, $createdEmail);

            $browser->pause(rand(10, 50));

            $year = $this->selectRandomYear($browser, $createdEmail);

            $browser->pause(rand(10, 50));

            $questionnaire->birthday = (new \DateTime())->setDate($year, $month, $day);

            //***     END BIRTHDAY

            usleep(200);
            $this->selectRandomGender($browser, $createdEmail, $questionnaire);

            $browser->pause(rand(10, 50));

            $questionnaire->question_text = $this->selectRandomQuestion($browser);

            $browser->pause(rand(10, 50));

            $questionnaire->question_answer = str_random(16);

            $browser->keys('input[name=answer]', $questionnaire->question_answer);

            $browser->pause(rand(10, 100));

            //$browser->waitUntil('[data-cerber-id="registration_form::mail::recaptcha"]');

            if(!$this->googleReCaptha($browser)) {
                Throw new \Exception('Google captcha is not resolved.');
            };

            $browser->element('[data-cerber-id="registration_form::mail::register_button"]')->click();

            $browser->screenshot('src0');

            $browser->pause(300);

            $browser->screenshot('src1');

            $browser->pause(300);

            $browser->screenshot('src2');

            $browser->pause(300);

            $browser->screenshot('src3');

            $browser->storeConsoleLog('phantomjs-console-log-' . date('Y_m_d_H_i_s'));

            $questionnaire->save();

            $createdEmail->createdQuestionnaire()->associate($questionnaire);
            $createdEmail->save();

            return true;
        });
    }

    protected function selectAvailEmail(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail)
    {
        echo 'selectAvailEmail' . PHP_EOL;
        //$browser->keys('input[name=login.username]', $faker->userName);

        $div_suggest = $browser->element('div[data-cerber-id="registration_form::mail::suggest"]');

        /** @var RemoteWebElement[] $lis */
        $lis = $div_suggest->findElements(WebDriverBy::tagName('span'));

        if(sizeof($lis) <= 0) return false;

        /** @var RemoteWebElement $li */
        $li = $lis[rand(0, sizeof($lis)-1)];

        $createdEmail->email = $li->getText();
        echo '.. email:' . $createdEmail->email . PHP_EOL;
        //echo $li->getText();
        $li->click();

        return true;
    }

    /**
     * @param \Laravel\Dusk\Browser $browser
     * @param CreatedEmail $createdEmail
     * @return null|string
     */
    protected function selectRandomDay(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail)
    {
        echo 'selectRandomDay' . PHP_EOL;
        $browser->click('input[name="birthday.day"]');

        $browser->pause(100);

        /** @var RemoteWebElement $birthday_day */
        $birthday_day = $browser->element('input[name="birthday.day"]');

        $birthday_day_par = $birthday_day->findElement(WebDriverBy::xpath('../../..'));

        $birthday_day_selections = $birthday_day_par->findElements(WebDriverBy::cssSelector('div div div div'));

        $avails_days = range(1, 31);
        $found_div_days = [];

        /** @var RemoteWebElement $div_day */
        foreach ($birthday_day_selections as $div_day) {
            $val = $div_day->getAttribute('innerHTML');

            if(in_array($val, $avails_days))
                $found_div_days[] = $div_day;
        }

        $index = rand(0, sizeof($found_div_days)-1);

        /** @var RemoteWebElement $div_day */
        $div_day = $found_div_days[$index];

        $div_day->click();
        echo '.. day:' . $div_day->getAttribute('innerHTML') . PHP_EOL;
        return $div_day->getAttribute('innerHTML');
    }

    /**
     * @param \Laravel\Dusk\Browser $browser
     * @param CreatedEmail $createdEmail
     * @return int
     */
    protected function selectRandomMonth(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail)
    {
        echo 'selectRandomMonth' . PHP_EOL;
        $browser->click('input[name="birthday.month"]');

        $browser->pause(100);

        /** @var RemoteWebElement $birthday_month */
        $birthday_month = $browser->element('input[name="birthday.month"]');

        $birthday_month_par = $birthday_month->findElement(WebDriverBy::xpath('../../..'));

        $birthday_months_selections = $birthday_month_par->findElements(WebDriverBy::cssSelector('div div div div'));

        $avails_monts = [
          'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
          'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь',
        ];
        $found_div_months = [];

        /** @var RemoteWebElement $div_month */
        foreach ($birthday_months_selections as $div_month) {
            $val = $div_month->getAttribute('innerHTML');

            if(in_array($val, $avails_monts))
                $found_div_months[] = $div_month;
        }

        $index = rand(0, sizeof($found_div_months)-1);

        /** @var RemoteWebElement $div_month */
        $div_month = $found_div_months[$index];

        $div_month->click();
        echo '.. month:' . ($index + 1) . PHP_EOL;
        return (int)($index + 1);
    }

    /**
     * @param \Laravel\Dusk\Browser $browser
     * @param CreatedEmail $createdEmail
     * @return int
     */
    protected function selectRandomYear(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail)
    {
        echo 'selectRandomYear' . PHP_EOL;
        $browser->click('input[name="birthday.year"]');

        $browser->pause(100);

        /** @var RemoteWebElement $birthday_year */
        $birthday_year = $browser->element('input[name="birthday.year"]');

        $birthday_year_par = $birthday_year->findElement(WebDriverBy::xpath('../../..'));

        $birthday_year_selections = $birthday_year_par->findElements(WebDriverBy::cssSelector('div div div div'));

        $avails_years = range(1940, 2000);
        $found_div_years = [];

        /** @var RemoteWebElement $div_year */
        foreach ($birthday_year_selections as $div_year) {
            $val = $div_year->getAttribute('innerHTML');

            if(in_array($val, $avails_years))
                $found_div_years[] = $div_year;
        }

        $index = rand(0, sizeof($found_div_years)-1);

        /** @var RemoteWebElement $div_year */
        $div_year = $found_div_years[$index];

        $div_year->click();
        echo '.. year:' . (int)$div_year->getAttribute('innerHTML') . PHP_EOL;
        return (int)$div_year->getAttribute('innerHTML');
    }

    /**
     * @param \Laravel\Dusk\Browser $browser
     * @param CreatedEmail $createdEmail
     * @param CreatedQuestionnaire $createdQuestionnaire
     * @return bool
     */
    protected function selectRandomGender(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail, CreatedQuestionnaire $createdQuestionnaire)
    {
        echo 'selectRandomGender' . PHP_EOL;
        $genderEl = $browser->element('input[name=gender]');

        $genderEl->click();

        $genderElPar = $genderEl->findElement(WebDriverBy::xpath('../../..'));

        $genderElSelections = $genderElPar->findElements(WebDriverBy::cssSelector('div div div div'));

        $genderAvails = ['Мужской', 'Женский'];
        $genderIndexs = [];
        $genderFounds = [];

        /** @var RemoteWebElement $genderElSelection */
        foreach ($genderElSelections as $genderElSelection) {
            $val = $genderElSelection->getAttribute('innerHTML');

            if(in_array($val, $genderAvails)) {
                $genderIndexs[] = $val;
                $genderFounds[] = $genderElSelection;
            }
        }

        $index = rand(0, sizeof($genderFounds)-1);

        $genderElSelection = $genderFounds[$index];

        $genderElSelection->click();

        $gender = $genderIndexs[$index];
        $createdQuestionnaire->gender = $gender == 'Мужской' ? CreatedQuestionnaire::GENDER_MALE : CreatedQuestionnaire::GENDER_FEMALE;
        echo '.. gender:' . $createdQuestionnaire->gender . PHP_EOL;
        return true;
    }

    /**
     * @param \Laravel\Dusk\Browser $browser
     * @return string
     */
    protected function selectRandomQuestion(\Laravel\Dusk\Browser $browser)
    {
        echo 'selectRandomQuestion' . PHP_EOL;
        $questionTypeEl = $browser->element('input[name=question]');

        $questionTypeEl->click();

        $questionTypeElPar = $questionTypeEl->findElement(WebDriverBy::xpath('../../..'));

        $questionTypeElSelections = $questionTypeElPar->findElements(WebDriverBy::cssSelector('div div div div'));

        $questionAvails = ['Почтовый индекс ваших родителей', 'Девичья фамилия матери', 'Модель вашей первой машины', 'Любимый писатель'];
        $questionIndexs = [];
        $questionFounds = [];

        /** @var RemoteWebElement $genderElSelection */
        foreach ($questionTypeElSelections as $questionTypeElSelection) {
            $val = $questionTypeElSelection->getAttribute('innerHTML');

            if(in_array($val, $questionAvails)) {
                $questionIndexs[] = $val;
                $questionFounds[] = $questionTypeElSelection;
            }
        }

        $index = rand(0, sizeof($questionFounds)-1);

        $questionTypeElSelection = $questionFounds[$index];

        $questionTypeElSelection->click();

        echo '.. question:' . $questionIndexs[$index] . PHP_EOL;
        return $questionIndexs[$index];
    }

    /**
     * По новому методу ruCaptcha
     *
     * @param \Laravel\Dusk\Browser $browser
     *
     * @return bool
     * @throws \Exception
     */
    protected function googleReCapthaRuCaptcha(\Laravel\Dusk\Browser $browser){
        $browser->waitFor('iframe');
        $browser->screenshot('cap0');
        $currentUrl = $browser->driver->getCurrentURL();
        $iframe = $browser->driver->findElement(WebDriverBy::cssSelector('iframe'));
        $src = $iframe->getAttribute('src');
        $parsed_url = parse_url($src);
        $getParams = [];
        parse_str($parsed_url['query'], $getParams);
        $googleKey = $getParams['k'];
        $browser->driver->switchTo()->frame($iframe);
        //$anchor = $browser->waitFor('#recaptcha-anchor-label');

        $id = $this->apiService->putReCaptchaV2New($googleKey, $currentUrl);

        echo 'Captcha ID: ' . $id . PHP_EOL;

        $code = null;
        $attempts = 30;

        do {
            echo date('Y-m-d H:i:s') . ' sleep 5 sec...' . PHP_EOL;
            sleep(5);

            $request = $this->apiService->getReCaptchaV2New($id);

            if($request !== ApiService::RUCAPTCHA_NOT_READY) {
                $code = $request;
                echo date('Y-m-d H:i:s') . ' cap resolved: ' . $code . PHP_EOL;
            }

        } while ($code == null && ($attempts-- > 0));

        $browser->driver->switchTo()->defaultContent();

        $browser->screenshot('cap1');

        $browser->script('document.getElementById(\'g-recaptcha-response\').style.display = \'block\';');
        $browser->script('document.getElementById(\'g-recaptcha-response\').value = \'' . $code . '\'');
        //$responseElement = $browser->driver->findElement(WebDriverBy::cssSelector('#g-recaptcha-response'));
        //$responseElement->sendKeys($code);

        $browser->screenshot('cap2');

        return $code !== null;
    }


    protected function emailIsPresent(Browser $browser)
    {
        ;
    }
}
