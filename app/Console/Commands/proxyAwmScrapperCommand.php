<?php

namespace App\Console\Commands;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\Proxy;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;

class proxyAwmScrapperCommand extends Command
{
  protected $browser;

  protected $logger;

  protected $host;

  protected $more = 1000;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'command:proxyScrapperAwm';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Scrap proxy list from proxy list.';

  /**
   * Create a new command instance.
   *
   * @param Browser $browser
   * @param LogToChannels $logger
   */
  public function __construct(Browser $browser, LogToChannels $logger)
  {
    parent::__construct();
    $this->browser = $browser;
    $this->logger  = $logger;
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    //https://checkerproxy.net/archive/2018-01-03
    $data = file_get_contents('https://awmproxy.net/freeproxy_7911567699.txt?');
    $strings = explode(PHP_EOL, $data);
    $collected = 0;
    foreach ($strings as $string) {
      $arr = explode(":", $string);
      $ip = $arr[0]; $port = $arr[1];
      $proxy = new Proxy([
        'ip'  => ip2long($ip),
        'port'=> (int)$port,
        'lastChecked' => date('Y-m-d H:i:s', 0)
      ]);
      $proxy->save();
      $collected++;
    }
    echo "collected " . $collected . PHP_EOL;
  }
}
