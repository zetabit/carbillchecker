<?php

namespace App\Console\Commands\avokod;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\Bill;
use App\Models\Car;
use App\Models\CreatedEmail;
use App\Services\Api\ApiService;
use App\User;
use Carbon\Carbon;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Console\Command;

class checkBillsCommand extends Command
{
    protected $browser;

    protected $logger;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:avtokod:checkBills {email?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check bills in cabinet for selected user.';

    protected $tmp_dir;

    protected $apiService;

    /**
     * Create a new command instance.
     *
     * @param Browser       $browser
     * @param LogToChannels $logger
     */
    public function __construct(Browser $browser, LogToChannels $logger, ApiService $apiService)
    {
        parent::__construct();
        $this->browser = $browser;
        $this->logger  = $logger;
        $this->apiService = $apiService;

        $this->tmp_dir = \Laravel\Dusk\Browser::$storeScreenshotsAt . DIRECTORY_SEPARATOR . 'tmp';

        \Laravel\Dusk\Browser::$storeScreenshotsAt .= DIRECTORY_SEPARATOR . 'avtokod';

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function handle()
    {
        //array_map('unlink', glob($this->tmp_dir.DIRECTORY_SEPARATOR."*"));
        //array_map('unlink', glob(\Laravel\Dusk\Browser::$storeScreenshotsAt.DIRECTORY_SEPARATOR."*"));
        $dir = storage_path('screenshots');
        $di = new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS);
        $ri = new \RecursiveIteratorIterator($di, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ( $ri as $file ) {
            $file->isDir() ?  rmdir($file) : unlink($file);
        }

        if(! file_exists($this->tmp_dir))
            mkdir($this->tmp_dir);

        if(! file_exists(\Laravel\Dusk\Browser::$storeScreenshotsAt))
            mkdir(\Laravel\Dusk\Browser::$storeScreenshotsAt);

        $this->browser->browse(function(\Laravel\Dusk\Browser $browser) {
            //TODO: select email from params and select car
            $user = User::with('createdEmails')->first();
            if ($this->hasArgument('email')) {
                $user = User::with('createdEmails')->whereEmail($this->argument('email'))->first();
            }

            /** @var CreatedEmail $email */
            foreach ($user->createdEmails as $createdEmail) {
                $this->loginAndCheck($browser, $createdEmail);
            }
        });
    }

    public function loginAndCheck(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail)
    {
        $browser
            ->visit('https://avtokod.mos.ru')
            ->clickLink('Войти')
            ->screenshot('1')
            ->waitFor('#submitButton')
            ->keys('[name="j_username"]', $createdEmail->email)
            ->keys('[name="j_password"]', $createdEmail->password)
            ->screenshot('2')
            ->click('#submitButton')
            ->screenshot('3')
            ->waitForText('Штрафы и')
            ->screenshot('4')
            ->clickLink('Проверить штрафы')
            ->waitFor('#fieldTc')
            ->waitForText('Свидетельство ТС')
            ->screenshot('5');

        /** @var Car $car */
        foreach ($createdEmail->cars as $car) {
            $browser
                ->keys('#fieldTc', $car->sts)
                ->pause(10)
                ->click('#btnAddTc')
                ->pause(10);
        }

        $browser
            ->click('#linkcheckData')
            ->pause(500);

        $this->checkCaptchaAndResolve($browser);
        usleep(100);

        $browser->waitFor('#loadProgress');
        $loadProgress = $browser->driver->findElement(WebDriverBy::id('loadProgress'));
        $attempts = 300;
        do {
            echo 'Штрафы грузятся.. '. date('Y-m-d H:i:s') . PHP_EOL;
            sleep(1);
        } while ($loadProgress->isDisplayed() && $attempts-- >= 0);

        if ($attempts <= 0) {
            throw new \Exception('Штрафы всё еще грузились :(');
        }

        $browser
            ->waitFor('#ProgressBarData.fines-table:not(.play)', 120)
            //->waitUntil('document.getElementsByClassName(\'.violation_status_loading\').length > 0')
            ->screenshot('6');

        $this->updateBills($browser, $createdEmail);
    }

    public function checkCaptchaAndResolve(\Laravel\Dusk\Browser $browser)
    {
        $winCaptcha = $browser->element('#winCaptcha');

        if (!$winCaptcha || ! $winCaptcha->isDisplayed())
            return; //TODO: log this event (block account attempts)

        $browser
            ->click('#btnReloadCaptcha')
            ->pause(1000);

        $imgCaptcha = $browser->element('#captchaImg');

        $size = $imgCaptcha->getSize();
        $coordinates = $imgCaptcha->getCoordinates()->onPage();
        $capName = 'captcha_' . date('Y_m_d_H_i_s');
        $browser->screenshot(DIRECTORY_SEPARATOR . $capName);
        $capPath = \Laravel\Dusk\Browser::$storeScreenshotsAt . DIRECTORY_SEPARATOR . $capName . '.png';

        while (!file_exists($capPath)) {
            usleep(500);
        }

        $srcCap = imagecreatefrompng($capPath);
        $destCap = imagecreatetruecolor($size->getWidth(), $size->getHeight());
        imagecopy($destCap, $srcCap, 0, 0, $coordinates->getX(), $coordinates->getY(), $size->getWidth(), $size->getHeight());
        if ($destCap) {
            imagefilter($destCap, IMG_FILTER_GRAYSCALE);
        }
        imagepng($destCap, $this->tmp_dir . DIRECTORY_SEPARATOR . $capName . '_cut.png');
        imagedestroy($destCap); imagedestroy($srcCap);
        $capPathFull = $this->tmp_dir . DIRECTORY_SEPARATOR . $capName . '_cut.png';

        $id = $this->apiService->putCaptcha(file_get_contents($capPathFull));
        echo 'Captcha ID: ' . $id . PHP_EOL;

        $code = null;
        $attempts = 30;

        do {
            echo date('Y-m-d H:i:s') . ' sleep 5 sec...' . PHP_EOL;
            sleep(5);

            $request = $this->apiService->getCaptcha($id);

            if($request !== ApiService::CAPCHA_NOT_READY) {
                $code = $request;
                echo date('Y-m-d H:i:s') . ' cap resolved: ' . $code . PHP_EOL;
            }

        } while ($code == null && ($attempts-- > 0));

        $inpCaptcha = $browser->element('#fieldCaptcha');
        $inpCaptcha->sendKeys($code);
        $browser->screenshot('keys');
        $btnCapthca = $browser->element('#fieldCaptchaImg');
        $btnCapthca->click();
        //TODO check captcha errors
    }

    public function updateBills(\Laravel\Dusk\Browser $browser, CreatedEmail $createdEmail)
    {
        //load all bills
        $driver = $browser->driver;
        $elements = [];

        $attempts = 100;
        do {
            $elements = $driver->findElements(WebDriverBy::cssSelector('.violation_status_loading'));
            echo 'Штрафов загружается.. ' . count($elements) . PHP_EOL;
            sleep(1);
        } while (count($elements) > 0 && $attempts-- >= 0);

        if ($attempts <= 0) {
            throw new \Exception('Штрафы всё еще подгружались :(');
        }

        $table  = $browser->driver->findElement(WebDriverBy::cssSelector('.APTablePanel'));
        $trs    = $table->findElements(WebDriverBy::cssSelector("tr"));

        /** @var Bill|null $bill */
        $bill = null;
        $oldUid = "";
        /** @var RemoteWebElement $tr */
        for ($i = 0, $c = sizeof($trs); $i < $c; $i++) {
            $tr = $trs[$i];
            $uid = $tr->getAttribute('data-uid');
            $dap = $tr->getAttribute('data-dap');

            if ($uid !== $oldUid && $dap) { //new bill item
                $oldUid = $uid;
                if ($bill) {
                    $bill->save();
                }

                $bill = Bill::firstOrNew([
                    'uid'   =>  $uid,
                    'number'=>  $dap,
                ]);

                $this->updHead($bill, $tr);
                $this->updDetails($bill, $trs[++$i]);
                ++$i;

                $bill->car()->associate($this->findCar($bill->ts, $createdEmail));
            }
        }

        //$rows = $browser->driver->findElements(WebDriverBy::cssSelector('.head-row'));
    }

    public function updHead(Bill $bill, RemoteWebElement $tr)
    {
        $class = $tr->getAttribute('class');
        $attempts = 100;
        do {
            $class = $tr->getAttribute('class');
            sleep(1);
            //echo $class . PHP_EOL;
        } while (substr_count($class, "violation_status_loading") > 0 && $attempts-- >= 0);

        if ($attempts <= 0) {
            throw new \Exception('ШтраФ всё еще подгружался :(');
        }

        if (substr_count($class, "violation_status_proccess")) {
            $bill->status = Bill::STATUS_PENDING;
        } elseif (substr_count($class, "violation_status_paid")) {
            $bill->status = Bill::STATUS_PAID;
        } elseif (substr_count($class, "violation_status_not_paid")) {
            $bill->status = Bill::STATUS_NOT_PAID;
        }

        $tds = $tr->findElements(WebDriverBy::cssSelector('td'));

        $date = Carbon::createFromFormat('d.m.Y', trim($tds[1]->getAttribute('textContent')))->format('Y-m-d');

        $bill->from = $date;

        $vd_tools = $tds[2]->findElements(WebDriverBy::cssSelector('.vd_tool'));

        $bill->type = $tds[4]->getAttribute('textContent');
        $isBill = strcmp($tds[4]->getAttribute('textContent'), "Штраф") === 0;

        $bill->price_no_discount = (double)$tds[6]->findElement(WebDriverBy::cssSelector('.no_discount'))->getAttribute('textContent');
        $bill->price_with_discount = (double)$tds[6]->findElement(WebDriverBy::cssSelector('.with_discount'))->getAttribute('textContent');
        //TODO: discount handle
        //$bill->date_time_discount_deadline = $tds[6]->findElement(WebDriverBy::cssSelector('.violation_deadline_date'))->getAttribute('textContent');
        $payBtn = $tds[6]->findElement(WebDriverBy::cssSelector('.violation_btn'));
    }

    public function updDetails(Bill $bill, RemoteWebElement $tr)
    {
        $table  = $tr->findElement(WebDriverBy::cssSelector('table'));
        $trs    = $table->findElements(WebDriverBy::cssSelector('tr'));

        /** @var RemoteWebElement $tr */
        for ($i = 0, $c = sizeof($trs); $i < $c; $i++) {
            $curTr  = $trs[$i];
            $tds    = $curTr->findElements(WebDriverBy::cssSelector('td'));

            if ($tds[0]->getAttribute('id') == "StAP") {
                $bill->StAP = $tds[1]->getAttribute('textContent');
            } elseif ($tds[0]->getAttribute('id') == "MestoDAP") {
                $bill->place_of_drawing = $tds[1]->getAttribute('textContent');
            } elseif ($tds[0]->getAttribute('id') == "DatNar") {
                $str = $tds[1]->getAttribute('textContent');
                $bill->date_time_violation = Carbon::createFromFormat('d.m.Y H:i:s', $str);
            } elseif ($tds[0]->getAttribute('id') == "MestoNar") {
                $bill->place_of_violation = $tds[1]->getAttribute('textContent');
            } elseif ($tds[0]->getAttribute('id') == "OdpsName") {
                $bill->authority = $tds[1]->getAttribute('textContent');
            } elseif ($tds[0]->getAttribute('id') == "FIONarush") {
                $bill->violator = $tds[1]->getAttribute('textContent');
            } elseif ($tds[0]->getAttribute('id') == "GRZNarush") {
                $bill->ts = $tds[1]->getAttribute('textContent');
            } elseif ($tds[0]->getAttribute('id') == "EffDate") {
                $str = $tds[1]->getAttribute('textContent');
                $bill->date_effective = Carbon::createFromFormat('d.m.Y', $str);
            }
        }
    }

    /**
     * @param              $partialNumber
     * @param CreatedEmail $createdEmail
     *
     * @return Car|null
     */
    public function findCar($partialNumber, CreatedEmail $createdEmail)
    {
        $partialNumber = mb_strtolower($partialNumber);

        /** @var Car[] $cars */
        $cars = $createdEmail->cars;

        $percents = [];
        $index = 0;

        /** @var Car $car */
        foreach ($cars as $car) {
            $number = mb_strtolower($car->number);
            $percent = similar_text($partialNumber, $number);
            $percents[] = [
                'number'    =>  $index++,
                'percent'   =>  $percent,
                'car'       => $car
            ];
        }

        array_multisort(array_column($percents, 'percent'), SORT_DESC, $percents);

        $car = null;

        foreach ($percents as $percent) {
            $car = $percent['car'];
            break;
        }

        return $car;
    }
}
