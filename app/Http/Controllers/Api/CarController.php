<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\AddCarRequest;
use App\Models\Car;
use App\Models\CreatedEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarController extends Controller
{
    const MAX_CARS_IN_EMAIL = 10;

    /**
     * Add Car to the system.
     *
     * @param AddCarRequest $request
     *
     * @return array [result => ok|fail]
     */
    public function addCar(AddCarRequest $request)
    {
        $emails = \Auth::user()->createdEmails()->get();

        $createdEmail = null;
        for ($i = 0, $c = sizeof($emails); $i < $c; $i++) { //try to find the free email
            /** @var CreatedEmail $curEmail */
            $curEmail = $emails[$i];
            if ($curEmail->cars->count() < self::MAX_CARS_IN_EMAIL) {
                $createdEmail = $curEmail;
                break;
            } else
                continue;
        }

        if (! $createdEmail) {
            return  [
                'result'    => 'fail',
                'reason'    => 'Not enough accounts'
            ];
        }

        $data = $request->only(['number', 'vin', 'sts']);

        if (!$data['vin']) $data['vin'] = "";

        $car = Car::create(
          array_merge($data, ['created_email_id' => $createdEmail->id])
        );
        return [
          'car_id'  => $car->id,
          'result'  => 'ok'
        ];
    }

    /**
     * @param $car_id int
     *
     * @return Car|null
     */
    public function getCar($car_id)
    {
        return Car::find($car_id);
    }

    public function getCars()
    {
        ;
    }
}
