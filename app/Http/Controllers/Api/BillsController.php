<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\GetBillsRequest;
use App\Models\Bill;
use App\Models\Car;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BillsController extends Controller
{
    public function getBillsForCar($number)
    {
        /** @var Car $car */
        $car    = Car::where('number',  '=',  $number)->first();

        if (!$car) {
            return [
                'result'    => 'fail',
                'reason'    => 'Car not found.'
            ];
        }

        return [
            'bills' => $car->bills,
            'result'=> 'ok'
        ];
    }

    public function getCarsBills()
    {
        /** @var Car $car */
        $cars    = Car::with('bills')->get();

        if (!$cars) {
            return [
                'result'    => 'fail',
                'reason'    => 'Cars not found.'
            ];
        }

        return [
            'cars' => $cars,
            'result'=> 'ok'
        ];
    }

    public function getBillsCar()
    {
        $bills = Bill::with('car')->get();

        if (!$bills) {
            return [
              'result'    => 'fail',
              'reason'    => 'Bils not found.'
            ];
        }

        return [
          'bills' => $bills,
          'result'=> 'ok'
        ];
    }
}
