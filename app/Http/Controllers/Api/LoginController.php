<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\AuthRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\RegToken;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

class LoginController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt-auth', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(AuthRequest $request)
    {
        $credentials = $request->only(  ['email', 'password']);
        $credentials['password'];

        // if (! $token = auth()->attempt($credentials)) {
        //     return response()->json(['error' => 'Unauthorized'], 401);
        // }

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials']);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        // return response()->json([
        //   'response' => 'success',
        //   'result' => [
        //     'token' => $token,
        //   ],
        // ]);

        return response()->json([
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function register(RegisterRequest $request)
    {
        $token = $request->get('reg_token');
        $email = $request->get('email');
        $password = Hash::make($request->get('reg_token'));

        /** @var RegToken $foundToken */
        $foundToken = RegToken::where('token', '=', $token)->first();

        if (! $foundToken) {
            return [
                'result'    => 'fail',
                'reason'    => 'Reg token does not exist.'
            ];
        }

        $user = User::create([
            'name'  => 'noname',
            'email' => $email,
            'password'  => $password
        ]);
        $user->password = $password;
        $user->save();

        $attempts = (int)$foundToken->attempts;
        if ($attempts > 1) {
            $foundToken->attempts = $attempts - 1;
            $foundToken->save();
        } else
            $foundToken->delete();

        $token = auth()->login($user);

        return [
            'result'    => 'ok',
            'user'      => $user,
            'token'     => $token
        ];
    }
}
