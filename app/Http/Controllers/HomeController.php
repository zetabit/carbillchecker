<?php

namespace App\Http\Controllers;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\Proxy;
use Carbon\Carbon;
use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Illuminate\Http\Request;
use League\Csv\Reader;

class HomeController extends Controller
{
  protected $browser;

  protected $logger;

  public function __construct(Browser $browser, LogToChannels $logger)
  {
    \Laravel\Dusk\Browser::$storeScreenshotsAt = storage_path();
    $this->browser = $browser;
    $this->logger  = $logger;
  }

  /**
   * @param Proxy|null $proxy
   * @return \Illuminate\Http\Response
   */
  public function index(Proxy $proxy = null)
  {
    /** @var Proxy $p */
    $p = $proxy;
    $screenShotName = date('Y-m-d-H-i-s');
    $screenShotPath = \Laravel\Dusk\Browser::$storeScreenshotsAt.DIRECTORY_SEPARATOR.$screenShotName.'.png';

    $proxyOptions = $p ? [
      'proxyType' => 'manual',
      'httpProxy' => long2ip($p->ip) . ':' . $p->port,
    ] : null;

    if($p && $p->https) {
      $proxyOptions['sslProxy'] = long2ip($p->ip) . ':' . $p->port;
    } elseif($p && ($p->version == 'socks4' || $p->version == 'socks5')) {
      $proxyOptions['socksProxy'] = long2ip($p->ip) . ':' . $p->port;
      $proxyOptions['socksVersion'] = (int)substr($p->version, -1);
    } elseif($p) {
    }

    $this->browser->browse(function ($browser) use ($screenShotName) {

      /** @var \Laravel\Dusk\Browser $browser */
      $browser->visit('https://old-linux.com/ip/');

      $browser->waitForLink('https://old-linux.com/ip/');

      //$browser->waitUntil(WebDriverExpectedCondition::titleIs('Quelle est mon IP ?'));

      $browser->screenshot($screenShotName);

      return true;
    },
      [
        'driver' => [
          'proxy' => $proxyOptions
        ]
      ]
    );


    if(!\File::exists($screenShotPath)) {
      abort(404);
    }

    $file = \File::get($screenShotPath);
    $type = \File::mimeType($screenShotPath);
    unlink($screenShotPath);

    $resp = \Response::make($file, 200);
    $resp->header("Content-Type", $type);

    return $resp;
  }
}
