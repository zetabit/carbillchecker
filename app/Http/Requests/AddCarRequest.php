<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'    =>  'required|unique:cars|max:64',
            //'vin'       =>  'required|unique:cars|max:64',
            'sts'       =>  'required|unique:cars|max:64'
        ];
    }
}
