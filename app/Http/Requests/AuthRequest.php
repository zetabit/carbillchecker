<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>  'required|string',
            'password'=>'required|string'
        ];
    }

    public function messages()
    {
        return [
            'email.required'    => 'Email is required.',
            'email.string'      => 'Email must be in email format.',
            'email.unique'      => 'Email is exists.',
            'password.required' => 'Password is required.',
            'password.string'   => 'Password must be a string.'
        ];
    }
}
