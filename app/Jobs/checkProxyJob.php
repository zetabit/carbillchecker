<?php

namespace App\Jobs;

use App\Browser;
use App\Helpers\LogToChannels;
use App\Models\Proxy;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class checkProxyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $proxy;

    public $tries = 6;

    public $timeout = 10;

  /**
   * Create a new job instance.
   *
   * @param Proxy $proxy
   */
    public function __construct(Proxy $proxy)
    {
      $this->proxy = $proxy;
      \Laravel\Dusk\Browser::$storeScreenshotsAt = storage_path();
    }

  /**
   * Execute the job.
   *
   * @param Browser $browser
   * @param LogToChannels $logger
   * @return void
   * @throws \Exception
   */
    public function handle(Browser $browser, LogToChannels $logger)
    {
      /** @var Proxy $proxy */
      $proxy = $this->proxy;
      $p = $proxy;
      $screenShotName = date('Y-m-d-H-i-s');
      $screenShotPath = \Laravel\Dusk\Browser::$storeScreenshotsAt.DIRECTORY_SEPARATOR.$screenShotName.'.png';

      $proxyOptions = $p ? [
        'proxyType' => 'manual',
        'httpProxy' => long2ip($p->ip) . ':' . $p->port,
      ] : null;

      if($p->https) {
        $proxyOptions['sslProxy'] = long2ip($p->ip) . ':' . $p->port;
      } elseif($p->version == 'socks4' || $p->version == 'socks5') {
        $proxyOptions['socksProxy'] = long2ip($p->ip) . ':' . $p->port;
        $proxyOptions['socksVersion'] = (int)substr($p->version, -1);
      } elseif($p) {
      }

      try {
        $browser->browse(function ($browser) use ($screenShotName, $proxy, $logger) {

          /** @var \Laravel\Dusk\Browser $browser */
          $browser->visit('https://old-linux.com/ip/');

          $browser->waitForLink('https://old-linux.com/ip/', 1);

          $browser->ensurejQueryIsAvailable();

          $ip = long2ip($proxy->ip);

          $script = <<<JS
            var font = jQuery.find("ul font");
            return font.length > 0 && jQuery(font).html() === '{$ip}';
JS;

          $bAnonymous = $browser->driver->executeScript($script);

          if($bAnonymous) {
            $proxy->status = 'active';
            $proxy->attempts = 0;
          } elseif ($proxy->attempts >= $this->tries)
            $proxy->status = 'disabled';
          else
            $proxy->attempts = $proxy->attempts + 1;

          $proxy->lastChecked = date('Y-m-d H:i:s');
          $proxy->save();

          $logger->info('checking_proxy', $ip . ' is ' . $proxy->status);

          //$browser->screenshot($screenShotName);

          return true;
        },
          [
            'driver' => [
              'proxy' => $proxyOptions
            ]
          ]
        );
      } catch (\Exception $exception) {
        $logger->err('checking_proxy_ex', long2ip($proxy->ip) . ' failed #' . $this->attempts() . ' check. ' . $exception->getMessage());

        $proxy->lastChecked = date('Y-m-d H:i:s');

        if ($proxy->attempts >= $this->tries)
          $proxy->status = 'disabled';
        else
          $proxy->attempts = $proxy->attempts + 1;

        $proxy->save();

        if(($this->attempts() > $this->tries) && ($proxy->attempts >= $this->tries))
          $this->fail($exception);
      }

      if($proxy->status == 'checking') {
        $this->release($this->attempts() * 10);
      }

    }
}
