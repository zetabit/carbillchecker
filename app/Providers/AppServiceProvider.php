<?php

namespace App\Providers;

use Alchemy\Zippy\Zippy;
use Illuminate\Support\ServiceProvider;
use JonnyW\PhantomJs\Client;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      $this->zippy();

      /*

      $this->client = Client::getInstance();
      $this->host = env('APP_URL');
      //$this->phantomTestLoad();
      */
    }

    protected function zippy()
    {
      $this->app->singleton('zippy', function () {
        $zippy = Zippy::load();
        // Give the PHP Zip Extension priority for zip files
        $zippy->addStrategy(
          new class($zippy->adapters) extends \Alchemy\Zippy\FileStrategy\ZipFileStrategy
          {
            protected function getServiceNames()
            {
              return [
                'Alchemy\\Zippy\\Adapter\\ZipExtensionAdapter'
              ];
            }
          }
        );
        return $zippy;
      });
      $this->app->alias('zippy', Zippy::class);
      $this->app->singleton(\App\DownloadsManager::class, function ($app) {
        return new \App\DownloadsManager(storage_path('downloads'), $app['zippy']);
      });
      $this->app->singleton(\App\Browser::class, function ($app) {

          \Laravel\Dusk\Browser::$storeScreenshotsAt = storage_path('screenshots');
          \Laravel\Dusk\Browser::$storeConsoleLogAt = storage_path('logs');
          if(!file_exists(\Laravel\Dusk\Browser::$storeScreenshotsAt)) mkdir(\Laravel\Dusk\Browser::$storeScreenshotsAt);
          if(!file_exists(\Laravel\Dusk\Browser::$storeConsoleLogAt)) mkdir(\Laravel\Dusk\Browser::$storeConsoleLogAt);

        return new \App\Browser($app[\App\DownloadsManager::class]);
      });
    }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function phantomTestLoad()
  {
    $request = $this->client->getMessageFactory()->createRequest($this->host, 'GET');

    $responce = $this->client->getMessageFactory()->createResponse();

    $this->client->send($request, $responce);

    if($responce->getStatus() === 200) {
      echo $responce->getContent();
    }
  }
}
