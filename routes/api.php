<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
  'as'      => 'api.',
  'prefix'  => '/v1',
], function() {
    Route::post('login', 'Api\LoginController@login')->name('login');
    Route::post('register', 'Api\LoginController@register')->name('register');
});

Route::group([
  'as'      => 'api.',
  'prefix'  => '/v1',
  'middleware' => 'jwt-auth',
], function() {
    Route::post('/addCar', 'Api\CarController@addCar')->name('addCar');
    Route::get('/getCar/{car_id}', 'Api\CarController@getCar')->name('getCar');

    Route::get('/getBillsForCar/{number}', 'Api\BillsController@getBillsForCar')->name('getBillsForCar');
    Route::get('/getCarsBills', 'Api\BillsController@getCarsBills')->name('getCarsBills');
    Route::get('/getBillsCar', 'Api\BillsController@getBillsCar')->name('getCarsBills');
});
