<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProxyTable extends Migration
{

  protected $table = 'proxies';

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create($this->table, function (Blueprint $table) {
          $table->increments('id');

          $table->bigInteger('ip');
          $table->smallInteger('port', false, true);
          $table->smallInteger('attempts', false, true)->default(0);
          $table->enum('status', ['active', 'checking', 'disabled'])->default('checking');
          $table->string('code', 2)->default("");
          $table->string('country')->default("");
          $table->enum('version', ['http', 'socks4', 'socks5']);
          $table->enum('anonymity', ['elite', 'anonymous', 'transparent'])->default('anonymous');
          $table->boolean('google')->default(false);
          $table->boolean('https')->default(false);
          $table->dateTime('lastChecked');

          $table->timestamps();
      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists($this->table);
  }
}
