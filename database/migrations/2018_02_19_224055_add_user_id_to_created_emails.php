<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToCreatedEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('created_emails', function (Blueprint $table) {
            $table->integer('user_id', false, true);
            $table->foreign('user_id')
              ->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('created_emails', function (Blueprint $table) {
            $table->dropForeign('created_emails_user_id_foreign');
            $table->dropColumn('user_id');
        });
    }
}
