<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');

            $table->string('uid')->unique();
            $table->decimal('number', 28, 0)->unique();
            $table->date('from');

            $table->string('StAP');             //Статья КоАП или закона субъекта РФ, состав правонарушения
            $table->string('place_of_drawing'); //Место составления документа
            $table->dateTime('date_time_violation'); //Дата и время нарушения
            $table->string('place_of_violation');//Место нарушения
            $table->string('authority');        //Орган власти, выявивший нарушение
            $table->string('violator');         //Нарушитель
            $table->string('ts');               //транспортное средство
            $table->date('date_effective');     //дата вступления в силу
            $table->string('type');             //тип

            $table->float('price_no_discount');
            $table->float('price_with_discount');
            $table->dateTime('date_time_discount_deadline');

            $table->enum('status', ['not_paid', 'pending', 'paid']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
