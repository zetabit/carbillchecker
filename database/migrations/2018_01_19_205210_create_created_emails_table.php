<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatedEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('created_emails', function (Blueprint $table) {
            $table->increments('id');

            $table->string('email')->unique();
            $table->string('password');
            $table->enum('source', ['mail_ru', 'rambler', 'google', 'yandex']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('created_emails');
    }
}
