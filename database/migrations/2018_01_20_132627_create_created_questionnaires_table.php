<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreatedQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('created_questionnaires', function (Blueprint $table) {
            $table->increments('id');

            $table->string('firstName');
            $table->string('lastName');
            $table->string('birthday');
            $table->string('gender');
            $table->string('district')->default("");
            $table->string('question_text');
            $table->string('question_answer');

            $table->timestamps();
        });

        Schema::table('created_emails', function (Blueprint $table) {
          $table->integer('created_questionnaire_id', false, true)->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('created_questionnaires');

        Schema::table('created_emails', function (Blueprint $table) {
          $table->dropColumn('created_questionnaire_id');
        });
    }
}
