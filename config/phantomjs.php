<?php

return [

    // phantomjs binary path
    'binary_path' => public_path().'/../bin/phantomjs213',

    // phantomjs run options, http://phantomjs.org/api/command-line.html
    'options' => [
      '--webdriver=9515',
      //'--proxy=http://fr.proxymesh.com:31280',
      //'--proxy-type=http',
    ],

    'pid_file'   => storage_path().'/phantomjs_pid',

    'log_file' => storage_path().'/logs/phantomjs.log',
];